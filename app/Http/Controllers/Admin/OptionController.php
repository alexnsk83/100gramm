<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Option;

class OptionController extends Controller
{
    public function edit()
    {
        $options = Option::first();
        $title = trans('site.button.options');

        return view('admin.pages.options', compact('options', 'title'));
    }

    public function editPost()
    {
        $this->validate($this->request, [
            'email' => 'required|email',
            'keywords' => 'required|min:2',
            'description' => 'required|min:2',
        ]);

        $check = Option::first();

        if(!$check) {
            $options = new Option();
            $options->email = $this->request->email;
            $options->keywords = json_encode($this->request->keywords);
            $options->description = json_encode($this->request->description);
            $options->save();
        } else {
            Option::where('id', $check->id)->update([
                'email' => $this->request->email,
                'keywords' => json_encode($this->request->keywords),
                'description' => json_encode($this->request->description),
            ]);
        }

        return redirect()->route('admin.options')->with('message', 'Изменения сохранены');
    }
}
