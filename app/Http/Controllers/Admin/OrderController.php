<?php

namespace App\Http\Controllers\Admin;

use App\File;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function showAll()
    {
        $orders= Order::all();
        $files = File::all();
        $title = trans('site.title.orders');

        return view('admin.pages.order_list', compact('orders', 'files', 'title'));
    }

    public function edit($id)
    {
        $order = Order::find($id);
        $title = "Заказ №$id";

        return view('admin.pages.order', compact('order', 'title'));
    }

    public function editPost()
    {

    }
}
