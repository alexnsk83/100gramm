<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module;
use App\Page;
use Illuminate\Support\Facades\Input;

class PageController extends Controller
{
    /** Отображение списка страниц
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $pages = Page::orderBy('sort_order', 'asc')->get();
        $title = trans('site.title.pages');

        return view('admin.pages.page_list', compact('pages', 'title'));
    }

    /** создание новой страницы
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $title = trans('site.title.page_add');
        $modules = Module::all();
        return view('admin.pages.page_add', compact('modules', 'title'));
    }

    /** Сохранение новой страницы
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addPost()
    {
        $this->validate($this->request, [
            'name' => 'required|unique:pages|max:255',
            'slug' => 'required|unique:pages|max:255',
            'sort_order' => 'required|integer',
        ]);

        $page = new Page();
        $page->name = $this->request->name;
        $page->slug = $this->request->slug;
        $page->text = $this->request->text;
        $page->sort_order = $this->request->sort_order;
        $page->module_id = $this->request->module;
        $page->menu_1 = $this->request->menu_1;
        $page->menu_2 = $this->request->menu_2;
        $page->keywords = $this->request->keywords;
        $page->description = $this->request->description;

        $page->save();

        return redirect()->route('admin.page.all')->with('message', trans('messages.page_created'));
    }

    /**
     * Редактирование страницы
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $page = Page::where('id', $id)->first();
        $title = $page->name;

        return view('admin.pages.page_edit', compact('page', 'title'));
    }

    /** Сохраннеие изменений страницы
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editPost($id)
    {
        $this->validate($this->request, [
            'name' => 'required|max:255',
            'sort_order' => 'required|integer',
        ]);

        if ($this->request->module === '1') {
            $this->validate($this->request, [
                'slug' => 'required',
                'text' => 'required',
            ]);
        }

        $page = Page::find($id);

        $update = $page->update([
            'name' => $this->request->name,
            'sort_order' => $this->request->sort_order,
            'menu_1' =>$this->request->menu_1,
            'menu_2' =>$this->request->menu_2,
            'slug' => $this->request->slug,
            'text' => $this->request->text,
            'keywords' => $this->request->keywords,
            'description' => $this->request->description,
        ]);

        if ($update){
            return redirect()->route('admin.page.all')->with('message', trans('messages.page_updated'));
        } else {
            return back();
        }
    }

    /** Удаление страницы
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Page::where('id', $id)->delete();

        return redirect()->route('admin.page.all')->with('message', trans('messages.page_deleted'));
    }
}
