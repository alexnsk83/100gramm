<?php

namespace App\Http\Controllers\Admin;

use App\Sale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaleController extends Controller
{
    /**
     * Отображает список акций
     * @param bool $trashed
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAll($trashed = false)
    {
        $sales = $trashed ? Sale::onlyTrashed()->get() : Sale::all();
        $title = trans('site.title.sales');

        return view('admin.pages.sale_list', compact('sales', 'title', 'trashed'));
    }

    /**
     * Отображает только удалённые акции
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTrashed()
    {
        return $this->showAll(true);
    }

    /**
     * Форма создания акции
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $title = trans('site.title.sales');
        return view('admin.pages.sale_add', compact('title'));
    }

    public function addPost()
    {
        $sale = new Sale();

        $this->validate($this->request, [
            'caption' => 'required|unique:posts|min:2',
            'detail' => 'required|min:2',
        ]);

        $sale->caption = $this->request->caption;
        $sale->detail = $this->request->detail;
        $sale->color = $this->request->color;

        $sale->save();

        return redirect()->route('admin.sale.all')->with('message', trans('messages.sale_created'));
    }

    /**
     * Форма редактирования акции
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $sale = Sale::withTrashed()
            ->where('id', $id)->first();
        $title = trans('site.title.sale_edit');

        return view('admin.pages.sale_add', compact('sale', 'title'));
    }

    /**
     * Сохранение изменений в акции
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editPost($id)
    {
        $this->validate($this->request, [
            'caption' => 'required|min:2',
            'detail' => 'required|min:2',
        ]);

        $sale = Sale::withTrashed()
            ->where('id', $id)->update([
                'caption' => $this->request->caption,
                'detail' => html_entity_decode($this->request->detail),
                'color' => $this->request->color,
            ]);

        if ($sale){
            return back()->with('message', trans('messages.sale_saved'));
        } else {
            return back();
        }
    }

    /**
     * Удаление акции
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Sale::where('id', $id)->delete();

        return redirect()->route('admin.sale.all')->with('message', trans('messages.sale_deleted'));
    }


    /**
     * Восстановление удалённой акции
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function recover($id)
    {
        Sale::where('id', $id)->restore();

        return redirect()->back()->with('message', trans('messages.sale_recovered'));
    }
}
