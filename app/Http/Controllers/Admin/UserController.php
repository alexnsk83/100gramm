<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UserStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\CheckAvatar;

class UserController extends Controller
{
    public function showAll()
    {
        $users = User::all();
        $title = trans('site.title.users');

        //Заменяем аватар заглушкой если файл аватара не найден
        foreach ($users as $user) {
            $user['avatar'] = CheckAvatar::isAvatarExists($user['avatar']);
        }

        return view('admin.pages.user_list', compact('users', 'title'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $user['avatar'] = CheckAvatar::isAvatarExists($user['avatar']);
        $statuses = UserStatus::all();
        $title = trans('site.title.user_edit');

        return view('admin.pages.user', compact('user', 'statuses', 'title'));
    }

    public function editPost()
    {
        User::where('id', $this->request->user_id)->update([
            'status' => $this->request->status,
        ]);

        $users = User::all();
        $title = trans('site.title.users');

        return redirect()->route('admin.user.all', compact('users', 'title'))->with('message', trans('messages.user_saved'));
    }

    public function delete($id)
    {
        if ($this->request->input('no')){
            return redirect()->route('admin.user.edit', $id);
        }

        User::where('id', $id)->delete();

        return redirect()->route('admin.user.all')->with('message', trans('messages.user_deleted'));
    }
}
