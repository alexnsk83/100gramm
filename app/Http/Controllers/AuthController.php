<?php

namespace App\Http\Controllers;


use App\Mail\UserRegistered;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class AuthController extends Controller
{
    public function register()
    {
        return view('pages.registration', [
            'title' => trans('site.title.register'),
        ]);
    }

    public function registerPost()
    {
        $user = new User();

        $this->validate($this->request, [
            'name' => 'required|unique:users|min:2|max:255',
            'password' => 'required',
            'password2' => 'required|same:password',
            'email' => 'required|email|unique:users|max:255',
            'avatar' => 'file|image',
        ]);

        if(is_file($_FILES['avatar']['tmp_name'])){
            Input::file('avatar')->move('assets/images/uploads/avatars/', $_FILES['avatar']['name']);
            $avatar = '/assets/images/uploads/avatars/' . $_FILES['avatar']['name'];
            $img = Image::make(public_path() . $avatar);
            $img->fit(150)->save(public_path() . '/assets/images/uploads/avatars/' . $_FILES['avatar']['name']);
        } else {
            $avatar = '/assets/images/uploads/avatars/user_avatar_empty.png';
        }

        $user->name = $this->request->name;
        $user->email = $this->request->email;
        $user->avatar = $avatar;
        $user->password = bcrypt($this->request->password);
        $user->created_at = $user->updated_at = Carbon::createFromTimestamp(time())->format('Y-m-d H:i:s');
        $user->save();

        Mail::to($this->request->email)
            ->send(new UserRegistered($this->request->name));

        return redirect()->route('site.auth.login')->with('message', trans('messages.registered'));
    }

    public function login()
    {
        return view('pages.login', [
            'title' => trans('site.title.login'),
        ]);
    }

    public function loginPost()
    {
        $remember = $this->request->input('remember') ? true : false;

        $authResult = Auth::attempt([
            'name' => $this->request->input('name'),
            'password' => $this->request->input('password'),
        ], $remember);

        if ($authResult) {
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('site.auth.login')->with('authError', 'Неправильный логин или пароль');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('site.home');
    }
}
