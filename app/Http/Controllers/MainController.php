<?php

namespace App\Http\Controllers;

use App\File;
use App\Option;
use App\Post;
use App\Order;
use App\Mail\OrderSent;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function index()
    {
        $posts = Post::all()->sortBy('order');
        $sales = Sale::all()->sortBy('order');
        $options = Option::first();
        return view('main', compact('posts', 'sales', 'options'));
    }

    public function sendForm()
    {
        $this->validate($this->request, [
            'name' => 'required|min:2',
            'telephone' => 'required|numeric',
            'email' => 'required|email',
            'address' => 'max:250',
        ]);

        if($this->request->confirm || $this->request->order != $this->request->comment){
            die('get lost');
        }


        $order = new Order();

        $order->name = json_encode($this->request->name);
        $order->telephone = $this->request->telephone;
        $order->email = $this->request->email;
        $order->address = json_encode($this->request->address);
        $order->order = json_encode($this->request->order);
        $order->save();

        $files = $this->request->file('file');
        if($files) {
            foreach ($files as $file){
                $orderFile = new File();
                $filename = $order->id . "_" . $this->request->telephone . "_" .
                    Carbon::now()->day . '-' . Carbon::now()->month . '-' . Carbon::now()->year . "_" .
                    Carbon::now()->micro . "." . $file->getClientOriginalExtension();
                $file->move(public_path() . '/assets/uploads/orders/', $filename);

                $orderFile->order_id = $order->id;
                $orderFile->filename = $filename;
                $orderFile->save();
            }
        }

        $mail = Option::first();
        if ($mail) {
            Mail::to($mail->email)->send(new OrderSent($order));
        } else {
            Mail::to('zakaz100gramm@mail.ru')->send(new OrderSent($order));
        }

        return redirect()->route('site.home')->with('message', 'Заявка отправлена');
    }
}
