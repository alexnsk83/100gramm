<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Page;

class PageController extends Controller
{
    /** Показ текстовой страницы
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPage($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        $title = $page->name;
        $description = $page->description;
        $keywords = $page->keywords;

        return view('pages.page', compact('page', 'title', 'description', 'keywords'));
    }
}
