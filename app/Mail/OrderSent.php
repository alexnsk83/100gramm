<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSent extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['file']){
            $mail = $this
                ->from(config('mail.from'))
                ->subject('Новый заказ')
                ->view('mail.new_order');
            foreach ($this->data['file'] as $file) {
                $mail->attach(public_path() . '/assets/uploads/orders/' . $file->filename);
            }
            return $mail;
        }

        return $this
            ->from(config('mail.from'))
            ->subject('Новый заказ')
            ->view('mail.new_order');
    }
}
