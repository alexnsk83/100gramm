<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App;
use App\Page;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale(App::getLocale());

        if (!app()->runningInConsole()) {
            $menu_1 = Page::where('menu_1', '1')->orderBy('sort_order', 'asc')->get();
            View::share('menu_1', $menu_1);
            $menu_2 = Page::where('menu_2', '1')->orderBy('sort_order', 'asc')->get();
            View::share('menu_2', $menu_2);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
