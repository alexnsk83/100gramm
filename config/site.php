<?php
return [
    'storagePath' => '/assets',
    'storageSection' => '/images/uploads',
    'sectionAvatars' => '/avatars/',
    'sectionPosts' => '/posts/',
    'storagePermissions' => 0755,
    'empty_avatar' => '/assets/images/uploads/avatars/user_avatar_empty.png'
];