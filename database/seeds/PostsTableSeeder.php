<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            'caption' => "Чайная суть",
            'order' => 1,
            'image' => "/assets/images/uploads/posts/leaf2.png",
            'detail' => '<p style="text-align:center"><u><span style="font-size:18.0pt">Чайная суть </span></u></p>

<p style="text-align:center"><u><span style="font-size:18.0pt">или </span></u></p>

<p style="text-align:center"><u><span style="font-size:18.0pt">Шесть плодов одного дерева:</span></u><br />
 </p>

<p><strong><span style="font-size:16.0pt">1. БОДРОСТЬ</span></strong><br />
<span style="font-size:16.0pt">Красный чай - это чай прошедший сильную ферментацию в процессе производства, для нас привычнее его называть чёрным чаем. Как правило, китайские красные чаи более мягкие по вкусу, и не имеют «индийской» терпкости. Бодрящий же эффект ощутим лишь в китайском красном чае, видимо это связано с процессом производства.<br />
Для производства качественного красного чая используют молодые чайные листья и почки. При этом, чем больше типсов (чайных почек) в чае, тем заметнее его бодрящий эффект. Типсовые красные чаи идеально подходят для хорошего начала дня!</span></p>

<p><strong><span style="font-size:16.0pt">2. ГАРМОНИЯ</span></strong><br />
<span style="font-size:16.0pt">Улун – это полуферментированный чай. Многообразие сортов этого чая является гордостью Китая. Светлые улуны обладают свойством гармонизировать общение за чаепитием. Помогают настроиться на единый лад. Идеально подходят <strong>для деловых переговоров</strong>, встреч и свиданий.</span></p>

<p><strong><span style="font-size:16.0pt">3. ОТДЫХ</span></strong><br />
<span style="font-size:16.0pt">Запечённые улуны обладают эффектом снятия эмоционального напряжения. Настраивают на свободное общение, долгие задушевные беседы. Идеально подходят для дружеской компании.</span></p>

<p><strong><span style="font-size:16.0pt">4. ТОНУС</span></strong><br />
<span style="font-size:16.0pt">Зелёный чай давно известен своими полезными свойствами. Для производства качественного зеленого чая используют молодые чайные листья. Идеально подходит для восстановления сил после физических нагрузок. Проясняет разум, раскрывает творческий потенциал. </span></p>

<p> </p>

<p><strong><span style="font-size:16.0pt">5. СКОРОСТЬ</span></strong><br />
<span style="font-size:16.0pt">Пуэр – это постферментированный чай, т.е. процессы ферментации проходит постепенно, в течение длительного времени. Поэтому ценятся пуэры выдержанные годами. Хороший пуэр определяется не только временем, но и качеством первоначального сырья и технологией изготовления. Пуэр бывает чёрным и зелёным.<br />
Чёрный Пуэр является отличным <strong>энергетиком</strong>. Имеет тёмный настой и мягкий вкус. В аромате могут быть ореховые, древесные или земляные нотки в зависимости от сорта. Повышает внимательность, работоспособность и активность в любое время суток. Также способствует усвоению пищи, улучшает обмен веществ, быстро снимает похмелье. </span></p>

<p><span style="font-size:16.0pt">Идеально подходит для выживания в большом городе.</span><br />
 </p>

<p><strong><span style="font-size:16pt"><span style="background-color:white">6. Динамичность. </span></span></strong></p>

<p><span style="font-size:16pt"><span style="background-color:white">Зелёный (Шен) пуэр Вкус «Шен пуэра» слегка подкопченный, с тонким оттенком спелого крыжовника. Иной раз присутствуют фруктовые нотки! </span></span><br />
<span style="font-size:16pt"><span style="background-color:white">И как хорошая традиция у этого чая - сладкое, немного вяжущие послевкусие. </span><br />
<span style="background-color:white">Этот чай многообразен и богат на оттенки вкусов, раскрывая их поэтапно во время заваривания. </span></span></p>

<p><span style="font-size:16pt"><span style="background-color:white">Его важно воспринимать как коллекционный чай, так как вкус «шен пуэра» со временем ощутимо меняется, приобретая новые нотки вкуса! </span></span></p>

<p><span style="font-size:16pt"><span style="background-color:white">Совсем опрометчиво выпить его задолго перед тем как добудете еще несколько экземпляров с других знаменитых гор или провинций!</span></span><br />
 </p>

<p style="text-align:center"><br />
<span style="font-size:16.0pt"><strong>Как правильно заварить вкусный ЧАЙ.</strong></span></p>

<ol>
	<li><strong><span style="font-size:16.0pt">Традиционный способ.</span></strong><br />
	<span style="font-size:16.0pt">Одним из традиционных способов приготовления чая является пролив. Этот способ как нельзя лучше подходит для заваривания китайских сортов чая. Для получения чая используют два сосуда: в одном заваривают чай, а затем переливают готовый настой во второй сосуд. Каждую последующую заварку настаивают чуть дольше предыдущей. </span></li>
	<li><strong><span style="font-size:16.0pt">Простой способ.</span></strong></li>
</ol>

<p><span style="font-size:16.0pt">Один из самых простых способов заварить чай – это заварить его прямо в кружку. Главное соблюсти тонкий момент первенства воды и чая: красные и чёрные чаи насыпают в кружку и заливают резким кипятком, а зелёные чаи и улуны лучше опустить в уже налитый в кружку кипяток. Количество заварки необходимое на кружку 2 грамма – обычно, это одна щепотка. </span></p>

<p> </p>',
            'is_visible' => 1,
        ]);

        Post::create([
            'caption' => "Любимый кофе",
            'order' => 2,
            'image' => "/assets/images/uploads/posts/coffee2.png",
            'detail' => '<p style="text-align:center"><strong><span style="font-size:16.0pt">Самый популярный напиток во всем Мире!</span></strong></p>

<p><span style="font-size:16.0pt">Мы продаем кофе класса </span><strong><span style="font-size:16.0pt">Specialty</span></strong><span style="font-size:16.0pt">. </span></p>

<p><span style="font-size:16.0pt">Кофе, представленный в нашем ассортименте можно <strong>варить</strong>, а можно <strong>заваривать</strong>.</span></p>

<ol>
	<li><strong><span style="font-size:16.0pt">Кофе подходящий для ВАРКИ.</span></strong></li>
</ol>

<p><span style="font-size:16.0pt">Эспрессо смеси и моносорта с обжаркой под эспрессо, порадуют вас своим балансом и яркостью вкуса. </span></p>

<ol>
	<li><strong><span style="font-size:16.0pt">Кофе подходящий для ЗАВАРКИ.</span></strong></li>
</ol>

<p><span style="font-size:16.0pt">Это <strong>моносорта</strong> для альтернативных способов приготовления.</span></p>

<p><span style="font-size:16.0pt">Вы полюбите нотки и оттенки во вкусе присущие каждому кофейному сорту, ощутить которые возможно только выбрав Заварку как способ приготовления!</span></p>

<p> </p>

<p style="text-align:center"><strong><span style="font-size:16.0pt">Как приготовить вкусный Кофе!</span></strong></p>

<p> </p>

<ol>
	<li><strong><span style="font-size:16.0pt">Кофе можно сварить: </span></strong></li>
</ol>

<p><span style="font-size:16.0pt">в Турке, Гейзере, Сифоне, Кофеварке. </span></p>

<p><span style="font-size:16.0pt">Советуем использовать кофе -</span></p>

<p><span style="font-size:16.0pt">Специально созданные для варки эспрессо смеси</span></p>

<p><span style="font-size:16.0pt">или моносорта с обжаркой под эспрессо.</span></p>

<p> </p>

<ol>
	<li><strong><span style="font-size:16.0pt">Кофе можно заварить! </span></strong></li>
</ol>

<p><span style="font-size:16.0pt">Это самый древний, быстрый и удобный способ приготовить кофе. </span></p>

<p><span style="font-size:16.0pt">На обычную КРУЖКУ или Френч-пресс, кемикс, аэропрес на 350 мл потребуется 2,5 – 3 чайные ложки с небольшой горкой молотого кофе. Измельчать кофе лучше всего непосредственно перед приготовлением.</span></p>

<p><span style="font-size:16.0pt">В предварительно прогретую посуду засыпьте кофе и залейте кипятком (100 градусов) с небольшого расстояния от ёмкости. Это позволит насытить воду кислородом!</span></p>

<p><span style="font-size:16.0pt">Спустя 30 секунд размешайте ложечкой КОФЕ. </span></p>

<p><span style="font-size:16.0pt">В Случае с <strong>Френч-прессом</strong>, поршень можно опустить. </span></p>

<p><span style="font-size:16.0pt">В <strong>Кемексе</strong> и <strong>Аэропресе</strong> кофе не размешивают.</span></p>

<p><span style="font-size:16.0pt">Сахар, сливки или мороженое по вкусу возможно добавить в последнею очередь.</span></p>

<p><strong><span style="font-size:16.0pt">Вуаля!</span></strong><span style="font-size:16.0pt"> – <em>Вкусный КОФЕ готов!</em></span></p>',
            'is_visible' => 1,
        ]);

        Post::create([
            'caption' => 'О нас',
            'order' => 4,
            'image' => "/assets/images/uploads/posts/logo.png",
            'detail' => '<p style="text-align:center"><span style="font-size:16.0pt">Мы существуем с 2010 года.</span></p>

<p style="text-align:center"><span style="font-size:16.0pt">По сей прекрасный день. Всё это время</span></p>

<p style="text-align:center"><span style="font-size:16.0pt">мы стремились к максимальному качеству</span></p>

<p style="text-align:center"><span style="font-size:16.0pt">и безупречности предлагаемой нами продукции.</span></p>

<p style="text-align:center"><span style="font-size:16.0pt">Индивидуальный подход к каждому покупателю</span></p>

<p style="text-align:center"><span style="font-size:16.0pt">Стал фундаментом, приносящим нам спелые плоды – Вас, наших постоянных клиентов.</span></p>

<p style="text-align:center"> </p>

<p style="text-align:center"><em>Директор «Чайной лавки 100 граммЪ»</em></p>

<p style="text-align:center"><em>Андрей Семёнов</em></p>

<p style="text-align:center"><em>Желает всем вкусного чая и ароматного кофе.</em></p>',
            'is_visible' => 1,
        ]);

        Post::create([
            'caption' => 'Как купить',
            'order' => 3,
            'image' => "/assets/images/uploads/posts/pot.png",
            'detail' => '<p><strong><span style="font-size:14.0pt">1.Оформить заказ – это просто:</span></strong></p>

<ul>
	<li><span style="font-size:14.0pt">1.Скачайте интересные вам МЕНЮ (Чая, Кофе или Посуды).</span></li>
	<li><span style="font-size:14.0pt">2.В МЕНЮ вы можете отметить приглянувшиеся вам позиции.</span></li>
	<li><span style="font-size:14.0pt">3. Обязательно сохраните сформированный вами заказ в МЕНЮ.</span></li>
	<li><span style="font-size:14.0pt">4. Заполните ФОРМУ для оформления заказа на сайте.</span></li>
	<li><span style="font-size:14.0pt">5.Прикрепите к заполненной ФОРМЕ все МЕНЮ с вашими заказами.</span></li>
	<li><span style="font-size:14.0pt">6. Отправьте ваш заказ нам нажатием кнопки «Отправить».</span></li>
	<li><span style="font-size:14.0pt">7. После обработки вашего заказа вам придет письмо-подтверждение от Чайной лавки «100 граммЪ».</span></li>
	<li><span style="font-size:14.0pt">8. В письме будут: </span></li>
</ul>

<p><span style="font-size:14.0pt">1.Перечислены все товары отмеченные вами в МЕНЮ. </span></p>

<p><span style="font-size:14.0pt">2.Конечная сумма к оплате с учетом выбранного способа получения вашей будущей покупки.</span></p>

<p><span style="font-size:14.0pt">3.Сроки доставки вашего заказа.</span></p>

<h1> </h1>

<h1><span style="font-size:14.0pt">2.О доставке</span></h1>

<p><span style="font-size:14.0pt">Товар доставляется в оговоренные с покупателем сроки.<br />
Работаем без выходных.<br />
Стоимость доставки по городу Новосибирску рассчитывается индивидуально в зависимости от Вашего местоположения.</span></p>

<p><span style="font-size:14.0pt">Дорогие Новосибирцы!<br />
Свои пожелания о доставки или самостоятельной возможности получить заказ в наших магазинах, возможно, написать в графе пожеланий при оформлении вашего заказа!</span></p>

<p><strong><u><span style="font-size:14.0pt">Адреса магазинов:</span></u></strong><br />
<strong><span style="font-size:14.0pt">1) ул. Державина д. 1 «Fun – Time» </span></strong><br />
<strong><span style="font-size:14.0pt">ОТКРЫТО круглосуточно -</span></strong><strong> </strong><strong><span style="font-size:14.0pt">Ночные звонки</span></strong><span style="font-size:14.0pt"> по тел: 8-913-208-69-98</span></p>

<p><strong><span style="font-size:14pt">2) Красный проспект д. 218/2 (2 этаж) «Планета Игр»</span></strong><br />
<strong><span style="font-size:14.0pt">открыты Каждый день! ПН – СБ 10:00 до 20:00, ВС 10:00 до 19:00 </span></strong></p>

<p><span style="font-size:14.0pt">Возможна отправка <strong>в любые города</strong> транспортными компаниями, минимальная сумма покупки 1000 рублей. </span></p>

<p><span style="font-size:14.0pt">Услуги ТК оплачиваются дополнительно согласно их тарифам.</span></p>

<p><span style="font-size:14.0pt">В страны ближнего и дальнего зарубежья отправка Покупок производится Почтой России.</span></p>

<p><strong><span style="font-size:14.0pt">3.Способы оплаты:</span></strong></p>

<p><span style="font-size:14.0pt">Во всех случаях, возможно, оплатить на карту Сбербанка. </span></p>

<p><span style="font-size:14.0pt">В случае получения заказа в нашем магазине возможна оплата наличными средствами.</span></p>',
            'is_visible' => 1,
        ]);
    }
}
