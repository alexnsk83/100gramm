<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminName = 'andrey';
        $adminMail = 'admin@admin.ru';
        $adminPass = 'andryxa1507';
        $adminAvatar = '/assets/images/uploads/avatars/user_avatar_empty.png';

        User::create([
            'name' => $adminName,
            'email' => $adminMail,
            'avatar' => $adminAvatar,
            'password' => bcrypt($adminPass),
            'status' => 1,
        ]);
    }
}
