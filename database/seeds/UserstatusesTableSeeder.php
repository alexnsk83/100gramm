<?php

use Illuminate\Database\Seeder;
use App\UserStatus;

class UserstatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserStatus::create([
            'status' => 'admin',
        ]);

        UserStatus::create([
            'status' => 'active',
        ]);

        UserStatus::create([
            'status' => 'ban',
        ]);
    }
}
