/**
 * Created by Алексей on 22.12.2017.
 */
$(function(){
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            if ($('#upbutton').is(':hidden')) {
                $('#upbutton').css({opacity : 1}).fadeIn(1000);
            }
        } else { $('#upbutton').stop(true, false).fadeOut(500); }
    });

    $('a[href^="#"]').on('click', function(event) {
        // отменяем стандартное действие
        event.preventDefault();

        var sc = $(this).attr("href"),
            dn = $(sc).offset().top;

        /*
         * sc - в переменную заносим информацию о том, к какому блоку надо перейти
         * dn - определяем положение блока на странице
         */

        $('html, body').animate({scrollTop: dn}, 1500);

        /*
         * 1000 скорость перехода в миллисекундах
         */
    });
});