<?php
return [
    'title' => [
        'main' => 'Главная',
        'register' => 'Регистрация',
        'login' => 'Авторизация',
        'admin' => 'Панель управления',
        'posts' => 'Записи',
        'orders' => 'Заказы',
        'users' => 'Пользователи',
        'pages' => 'Страницы',
        'post_new' => 'Новая запись',
        'post_edit' => 'Редактироание записи',
        'user_edit' => 'Редактироание пользователя',
        'page_add' => 'Новая страница',
        'gallery' => 'Галерея',
        'sales' => 'Акции',
        'sale_edit' => 'Редактирование акции',
    ],

    'button' => [
        'register' => 'Зарегистрироваться',
        'login' => 'Войти',
        'logout' => 'Выйти',
        'admin' => 'Панель управления',
        'posts' => 'Записи',
        'orders' => 'Заказы',
        'users' => 'Пользователи',
        'pages' => 'Страницы',
        'sale' => 'Акции',
        'modules' => 'Модули',
        'to_site' => 'Вернуться на сайт',
        'main' => 'Главная',
        'delete' => 'Удалить',
        'save' => 'Сохранить',
        'save_and_exit' => 'Сохранить и закрыть',
        'edit' => 'Редактировать',
        'cancel' => 'Отмена',
        'yes' => 'Да',
        'no' => 'Нет',
        'back' => 'Назад',
        'read_more' => 'Читать далее',
        'post_new' => 'Новая запись',
        'post_edit' => 'Редактировать татью',
        'post_delete' => 'Удалить запись',
        'post_recover' => 'Восстановить запись',
        'page_new' => 'Добавить страницу',
        'page_edit' => 'Редактировать страницу',
        'page_delete' => 'Удалить страницу',
        'sale_new' => 'Новая акция',
        'show_trashed' => 'Показать удалённые',
        'show_active' => 'Показать активные',
        'albums' => 'Альбомы',
        'photos' => 'Фотографии',
        'gallery' => 'Галерея',
        'gallery_new' => 'Новая галерея',
        'gallery_edit' => 'Редактировать галерею',
        'gallery_delete' => 'Удалить галерею',
        'gallery_trashed' => 'Показать удалённые',
        'gallery_active' => 'Показать активные',
        'gallery_recover' => 'Восстановить галерею',
        'options' => 'Настройки',
    ],

    'form' => [
        'name' => 'Имя пользователя',
        'password' => 'Пароль',
        'password_repeat' => 'Повторите пароль',
        'email' => 'Почта',
        'remember' => 'Запомнить меня',
        'publish' => 'Опубликовать',
        'article_edit' => 'Редактирование записи',
        'article_name' => 'Название записи',
        'article_detail' => 'Текст записи',
        'article_photo' => 'Изображение к записи',
        'keywords' => 'Ключевые слова',
        'description' => 'Описание',
        'delete' => 'Удалить',
        'show_comments' => 'Показать комментарии',
        'commentable' => 'Разрешить комментировать',
        'gallery_edit' => 'Редактирование галереи',
        'gallery_new' => 'Новая галерея',
        'gallery_name' => 'Название галереи',
        'gallery_description' => 'Описание галереи',
        'gallery_cover' => 'Изображение обложки',
        'content_images' => 'Изображения',
        'multiple_load' => 'Можно загружать несколько файлов сразу',
        'order' => 'Порядковый номер',
        'sale_edit' => 'Редактирование акции',
        'sale_name' => 'Название акции',
        'sale_detail' => 'Текст акции',
        'options' => 'Настройки',
    ],

    'table' => [
        'avatar' => 'Аватар',
        'username' => 'Имя пользователя',
        'name' => 'Имя',
        'email' => 'Почта',
        'telephone' => 'Телефон',
        'address' => 'Адрес',
        'order' => 'Заказ',
        'order_number' => 'Номер заказ',
        'files' => 'Файлы',
        'status' => 'Статус',
        'created_at' => 'Создан',
    ],

    'dialog' => [
        'delete_user' => 'Удалить пользователя?',
        'delete_post' => 'Удалить запись?',
        'delete_gallery' => 'Удалить галерею?',
        'delete_sale' => 'Удалить акцию?',
        'sure' => 'Вы уверены?'
    ]
];