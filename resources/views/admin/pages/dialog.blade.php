@extends('admin.base')

@section('content')
    <div class="boxed  push-down-60">
        <div class="row">
            <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
                <div class="post-content">
                 <h2>{{ $question }}</h2>
                    <form method="post">
                        {{ csrf_field() }}
                        <input type="submit" name="yes" class="btn btn-primary" value="Да">
                        <input type="submit" name="no" class="btn btn-danger" value="Нет">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection