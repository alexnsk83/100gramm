@extends('admin.base')

@section('content')
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-bookmarks-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Записей</span>
                        <span class="info-box-number">{{ \App\Post::count() }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Пользователей</span>
                        <span class="info-box-number">{{ \App\User::count() }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
                <!-- USERS LIST -->
                <div class="box box-danger">
                    <!-- /.box-header -->
                    <!-- /.box-body -->
                    <!-- /.box-footer -->
                </div>
                <!--/.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection