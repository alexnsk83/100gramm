@extends('admin.base')

@section('content')
    <div class="container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>{{ trans('site.form.options') }}</h1>
            <form enctype="multipart/form-data" method="post">
                {{ csrf_field() }}

                <span>{{ trans('site.form.email') }}</span>
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->get('email')[0] }} </span>
                @endif<br>
                <input type="text" class="form-control" name="email"
                       @if (isset($options['email']))
                       value="{{ $options['email'] }}"
                       @else
                       value="{{ old('email') }}"
                        @endif><br>

                <span>{{ trans('site.form.keywords') }}</span>
                @if ($errors->has('keywords'))
                    <span class="text-danger">{{ $errors->get('keywords')[0] }} </span>
                @endif<br>
                <input type="text" class="form-control" name="keywords"
                       @if (isset($options['keywords']))
                       value="{{ json_decode($options['keywords']) }}"
                       @else
                       value="{{ old('keywords') }}"
                        @endif><br>

                <span>{{ trans('site.form.description') }}</span>
                @if ($errors->has('description'))
                    <span class="text-danger">{{ $errors->get('description')[0] }} </span>
                @endif<br>
                <textarea name="description" class="form-control" rows="15">@if (isset($options['description'])){{ json_decode($options['description']) }}@endif</textarea><br>

                <input type="submit" value="{{ trans('site.button.save') }}"  class="btn btn-primary">
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection