@extends('admin.base')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="box">
                    <div class="box-body">
                        <p>
                            <span class="text-bold">{{ trans('site.table.order_number') }}: </span> {{ $order['id'] }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.name') }}: </span> {{ json_decode($order['name']) }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.telephone') }}: </span> {{ $order['telephone'] }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.email') }}: </span> {{ $order['email'] }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.address') }}: </span> {{ json_decode($order['address']) }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.order') }}: </span> {{ json_decode($order['order']) }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.created_at') }}: </span> {{ $order['created_at'] }}
                        </p>
                        <p>
                            <span class="text-bold">{{ trans('site.table.files') }}: </span><br>
                            @foreach ($order['files'] as $file)
                                <!--<a href="{{ URL::to('assets/uploads/orders/' . $file->filename) }}">{{ $file->filename }}</a><br>-->
                                    <a href="{{ URL::to('assets/uploads/orders/' . $file->filename) }}">{{ $file->filename }}</a><br>
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
