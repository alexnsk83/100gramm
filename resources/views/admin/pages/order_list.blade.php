@extends('admin.base')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="box">
                    <div class="box-body">
                        <table class="table table-condensed table-hover">
                            <thread>
                                <tr>
                                    <th>№</th>
                                    <th>{{ trans('site.table.name') }}</th>
                                    <th>{{ trans('site.table.telephone') }}</th>
                                    <th>{{ trans('site.table.email') }}</th>
                                    <th>{{ trans('site.table.address') }}</th>
                                    <th>{{ trans('site.table.files') }}</th>
                                    <th>{{ trans('site.table.created_at') }}</th>
                                </tr>
                            </thread>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order['id'] }}</td>
                                    <td>{{ json_decode($order['name']) }}</td>
                                    <td>{{ $order['telephone'] }}</td>
                                    <td>{{ $order['email'] }}</td>
                                    <td>{{ json_decode($order['address']) }}</td>
                                    <td>{{ count($order->files()->get()) }}</td>
                                    <td>{{ $order['created_at'] }}</td>
                                    <td>
                                        <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.edit') }}"  href="{{ route('admin.order.edit', ['id' => $order['id']]) }}"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection