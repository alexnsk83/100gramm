@extends('admin.base')

@section('content')
    <div class="container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>{{ trans('site.form.sale_edit') }}</h1>
            <form enctype="multipart/form-data" method="post">
                {{ csrf_field() }}

                <span>Цвет фона</span><br>
                <input type="text" id="colorpicker" name="color" class="form-control"
                       @if (isset($sale['color']))
                       value="{{ $sale['color'] }}"
                       @else
                       value="{{ old('color') }}"
                        @endif><br>
                <script>
                    $("#colorpicker").spectrum({
                        preferredFormat: "hex",
                        showInput: true,
                        allowEmpty: true,
                        showAlpha: true
                    });
                </script><br>

                <span>{{ trans('site.form.sale_name') }}</span>
                @if ($errors->has('caption'))
                    <span class="text-danger">{{ $errors->get('caption')[0] }} </span>
                @endif<br>
                <input type="text" class="form-control" name="caption"
                       @if (isset($sale['caption']))
                       value="{{ $sale['caption'] }}"
                       @else
                       value="{{ old('caption') }}"
                        @endif><br>

                <span>{{ trans('site.form.sale_detail') }}</span>
                @if ($errors->has('detail'))
                    <span class="text-danger">{{ $errors->get('detail')[0] }} </span>
                @endif<br>
                <textarea name="detail" class="form-control" rows="15">@if (isset($sale['detail'])){{ trim($sale['detail']) }}@endif</textarea><br>
                <script src="\vendor\unisharp\laravel-ckeditor\ckeditor.js"></script>
                <script>
                    CKEDITOR.replace( 'detail', {
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                    } );
                </script>

                <input type="submit" value="{{ trans('site.button.save') }}"  class="btn btn-primary">
                @if (isset($sale['deleted_at']))
                    <a class="btn btn-primary" href="{{ route('admin.sale.trashed') }}">{{ trans('site.button.back') }}</a>
                @else
                    <a class="btn btn-primary" href="{{ route('admin.sale.all') }}">{{ trans('site.button.back') }}</a>
                @endif
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection