@extends('admin.base')

@section('content')
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('admin.sale.add') }}" class="btn btn-primary margin-top">{{ trans('site.button.sale_new') }}</a>
                @if ($trashed)
                    <a href="{{ route('admin.sale.all') }}" class="btn btn-primary margin-top">{{ trans('site.button.show_active') }}</a>
                @else
                    <a href="{{ route('admin.sale.trashed') }}" class="btn btn-primary margin-top">{{ trans('site.button.show_trashed') }}</a>
                @endif
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @foreach($sales as $sale)
                    <div class="box">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9">
                                <h3>{{ $sale['caption'] }}</h3>
                                <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.edit') }}"
                                   href="{{ route('admin.sale.edit', ['id' => $sale['id']]) }}"></a>
                                @if ($sale->trashed())
                                    <a class="admin-ctrl-btn admin-btn fa fa-magic" title="{{ trans('site.button.recover') }}"
                                       href="{{ route('admin.sale.recover', ['id' => $sale['id']]) }}"></a>
                                @else
                                    <a class="admin-ctrl-btn admin-btn fa fa-trash-o" title="{{ trans('site.button.delete') }}"
                                       href="{{ route('admin.sale.delete', ['id' => $sale['id']]) }}"
                                       onclick="return confirm('{{ trans('site.dialog.delete_sale') }}')"></a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection