@extends('admin.base')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="box">
                    <div class="box-body">
                        <table class="table table-condensed table-hover">
                            <thread>
                                <tr>
                                    <th>{{ trans('site.table.username') }}</th>
                                    <th>{{ trans('site.table.email') }}</th>
                                    <th>{{ trans('site.table.status') }}</th>
                                    <th>{{ trans('site.table.created_at') }}</th>
                                </tr>
                            </thread>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user['name'] }}</td>
                                    <td>{{ $user['email'] }}</td>
                                    <td>{{ DB::table('user_statuses')->where('id', $user['status'])->first()->status }}</td>
                                    <td>{{ $user['created_at'] }}</td>
                                    <td>
                                        <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.edit') }}"  href="{{ route('admin.user.edit', ['id' => $user['id']]) }}"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection