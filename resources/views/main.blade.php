<!doctype html>
<html lang="en">
<head>
    <meta name="yandex-verification" content="f67000655b0887af" />
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="theme-color" content="#d7ad49">
    <meta name="description" content="{{ json_decode($options['description']) }}">
    <meta name="keywords" content="{{ json_decode($options['keywords']) }}">
    <title>Чайная лавка "100 граммъ"</title>
    <link rel="icon" href="assets/images/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/styles.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<header id="top">
    @if (Session::has('message'))
        <div id="myModal" class="modal d-block">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('.close-btn').on('click', function () {
                $('#myModal').removeClass('d-block');
            })
        </script>
    @endif
    <div class="container mt-4">
        <div class="row">
            <div class="col-3">
                <div class="logo">
                    <img src="{{ URL::to('assets/images/logo.png') }}">
                </div>
            </div>
            <div class="menu col-6">
                <div class="nav">
                    @foreach($posts as $post)
                        @if($post['is_visible'])
                            <a href="#post_{{ $post['id'] }}">{{ $post['caption'] }}</a>
                        @endif
                    @endforeach
                </div>
                <br>
                <a target="_blank" href="tel:+79133879200"><div class="telephone">8-913-387-9200 <i class="fas fa-phone"></i></div></a><br>
                <a target="_blank" href="https://api.whatsapp.com/send?phone=79963815296"><div class="telephone">Мы в WhatsApp <i class="fab fa-whatsapp"></i></div></a>
            </div>
            <div class="col-3 right-col">
                <a href="#ya-map" class="map-link"><img src="{{ URL::to('assets/images/map_point.png') }}"></a>
                <a href="https://www.youtube.com/channel/UCqpZx5VQ4PfGL1NsayKzSkw" target="_blank" class="tube-link"><img src="{{ URL::to('assets/images/youtube.png') }}"></a>
                <a href="https://vk.com/lavka100gramm" target="_blank" class="vk-link" ><img src="{{ URL::to('assets/images/vk.png') }}"></a>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container">
        {{--Меню файлов--}}
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-12  col-md-6 download-list mt-2 p-2">
                <div>
                    <h3>Твоё меню</h3>
                    <span>формат excel</span>
                </div>
                <ul>
                    <li class="p-1">
                        <a href="{{ URL::to('docs/tea.xlsx') }}"><img src="{{ URL::to('assets/images/leaf.png') }}">Лучшие сорта чая <i class="far fa-arrow-alt-circle-down fa-sm" aria-hidden="true"></i></a>
                        <a target="_blank" href="https://drive.google.com/open?id=1z4sb0aCYEQU8UuoS7lYIoSaMm_o_UV5iQyihWOmAPkQ"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </li>
                    <li class="p-1">
                        <a href="{{ URL::to('docs/coffee.xlsx') }}"><img src="{{ URL::to('assets/images/coffee.png') }}">Кофе класса speciality <i class="far fa-arrow-alt-circle-down fa-sm" aria-hidden="true"></i></a>
                        <a target="_blank" href="https://drive.google.com/open?id=1dm9sUkOdv2VjgBcfq1E87WyCyhm9ZJQ8bAx4flRZGU8"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </li>
                    <li class="p-1"><a href="{{ URL::to('docs/acc.xlsx') }}"><img src="{{ URL::to('assets/images/pot.png') }}">Посуда и принадлежности <i class="far fa-arrow-alt-circle-down fa-sm" aria-hidden="true"></i></a>
                        <a target="_blank" href="https://drive.google.com/open?id=1zfxAxXHkOxfH5L1nt14wAKg8jnEy5k6f"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                    {{--<li class="p-1"><a href="{{ URL::to('docs/partners.xlsx') }}"><img src="{{ URL::to('assets/images/partner.png') }}">Предложение о партнерстве <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a></li>--}}
                </ul>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>

    @if (count($sales) > 0)
    {{--Список акций--}}
    <div class="container sales_block">
        <button class="btn sales_button collapsed"
                type="button"
                data-toggle="collapse"
                data-target=".sales_list"
                aria-expanded="false">
            Акции сегодня &nbsp;
        </button>

        <div class="sales_list collapse">
            @foreach($sales as $sale)
            <div class="row sales" style="background-color: {{ $sale['color'] }};">
                <div class="col-12">
                    <h2>{{ $sale['caption'] }}</h2>
                    <p>{!! $sale['detail'] !!}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif

    <div class="container">
        {{-- Форма заказа --}}
        <div class="row">
            <div class="col-12">
                <div class="form col-8 col-sm-8 col-md-6 col-lg-4 col-xl-4">
                    <form enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="form-header mb-2">
                            <h2>Заполните форму</h2>
                            <p>для оформления</p>
                            <p>Вашего заказа</p>
                        </div>
                        <input class="form-control" type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                        <input class="form-control" type="tel" name="telephone" placeholder="Телефон" value="{{ old('telephone') }}">
                        <input class="form-control" type="email" name="email" placeholder="Почта" value="{{ old('email') }}">
                        <input class="form-control" type="text" name="address" placeholder="Адрес доставки" value="{{ old('address') }}">
                        <textarea  id="order" class="form-control" name="order" placeholder="Напишите заказ или прикрепите его в файле" value="{{ old('order') }}"></textarea>
                        <input class="form-control" name="file[]" type="file" multiple value="{{ old('file') }}">
                        <input type="checkbox" name="confirm" class="hidden">
                        <textarea id="comment" name="comment" style="display: none"></textarea>
                        <input class="form-control" type="submit">
                    </form>
                    <script type="text/javascript">
                        $('#order').keyup('#comment', function () {
                            $('#comment').val($('#order').val());
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        @foreach ($posts as $post)
            @if($post['is_visible'])
        <div class="item col-9 col-sm-8" id="post_{{ $post['id'] }}">
            <h3>{{ $post['caption'] }} <img class="wp-post-image" src="{{ $post['image'] }}" alt="image"></h3>
            <p>{!! $post['detail'] !!}</p>
        </div>
            @endif
        @endforeach

    </div>
    <div class="container-fluid mt-4 mb-1" id="ya-map">
        <div class="map_point2"><img src="{{ URL::to('assets/images/map_point2.png') }}"></div>
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A09aef0c49cf4adfb0ac4a63264ead5ec8174b1aea6f6a2e9613c444b7eeda597&amp;width=100%25&amp;height=700&amp;lang=ru_RU&amp;scroll=false"></script>
    </div>
</main>
<a href="#top" id="upbutton" class="scrolltop_button"></a>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'OvRlKDpXJG';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="assets/js/scripts.js"></script>
<script src="https://use.fontawesome.com/d7477d32f9.js"></script>
</html>