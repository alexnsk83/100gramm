<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Чайная лавка "100 граммъ"</title>
    <link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/styles.css') }}">
    <script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script src="https://use.fontawesome.com/d7477d32f9.js"></script>
</head>
<body>
<header>
    @if (Session::has('message'))
        <div id="myModal" class="modal d-block">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('.close-btn').on('click', function () {
                $('#myModal').removeClass('d-block');
            })
        </script>
    @endif
    <div class="container mt-4">
        <div class="row">
            <div class="col-3">
                <div class="logo">
                    <img src="{{ URL::to('assets/images/logo.png') }}">
                </div>
            </div>
            <div class="menu col-6">
                <div class="nav">
                    <a href="#about-tea">Чай</a>
                    <a href="#about-coffee">Кофе</a>
                    <a href="#about-us">О нас</a>
                    <a href="https://vk.com/lavka100gramm" target="_blank"><img src="{{ URL::to('assets/images/vk.png') }}"></a>
                    <a href="#"><img src="{{ URL::to('assets/images/youtube.png') }}"></a>
                </div>
                <br>
                <div class="telephone">8 905 902 9090</div>
                <a href="#"><img class="whatsapp" src="{{ URL::to('assets/images/whatsapp.png') }}"></a>
            </div>
            <div class="col-3">
                <div class="map_point">
                    <a href="#ya-map"><img src="{{ URL::to('assets/images/map_point.png') }}"></a>
                </div>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col-9 col-md-6 download-list mt-2 p-2">
                <ul>
                    <a href="{{ URL::to('docs/tea.xlsx') }}"><li class="p-1"><img src="{{ URL::to('assets/images/leaf.png') }}">Лучшие сорта чая <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></li></a>
                </ul>
                <ul>
                    <a href="{{ URL::to('docs/coffee.xlsx') }}"><li class="p-1"><img src="{{ URL::to('assets/images/coffee.png') }}">Кофе класса speciality <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></li></a>
                </ul>
                <ul>
                    <a href="{{ URL::to('docs/acc.xlsx') }}"><li class="p-1"><img src="{{ URL::to('assets/images/pot.png') }}">Посуда и принадлежности <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></li></a>
                </ul>
                <ul>
                    <a href="{{ URL::to('docs/partners.xlsx') }}"><li class="p-1"><img src="{{ URL::to('assets/images/partner.png') }}">Предложение о партнерстве <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></li></a>
                </ul>
            </div>
            <div class="col-3 mt-2">
                <div class="p-2 banner">
                    <p style="font-size: 20px">АКЦИЯ</p>
                    <p style="font-size: 15px">Красный чай</p>
                    <p style="font-size: 15px">"Тонкий ирис"</p>
                    <p style="font-size: 25px">-30%</p>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form col-8 col-sm-8 col-md-6 col-lg-4 col-xl-4">
                <form enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <div class="form-header mb-2">
                        <h2>Заполните форму</h2>
                        <p>для оформления</p>
                        <p>Вашего заказа</p>
                    </div>
                    <input class="form-control" type="text" name="name" placeholder="Имя" value="{{ old('name') }}">
                    <input class="form-control" type="tel" name="telephone" placeholder="Телефон" value="{{ old('telephone') }}">
                    <input class="form-control" type="email" name="email" placeholder="Почта" value="{{ old('email') }}">
                    <input class="form-control" type="text" name="address" placeholder="Адрес доставки" value="{{ old('address') }}">
                    <textarea class="form-control" name="order" placeholder="Напишите заказ или прикрепите его в файле" value="{{ old('order') }}"></textarea>
                    <input class="form-control" name="file" type="file" value="{{ old('file') }}">
                    <input class="form-control" type="submit">
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="item col-9 col-sm-8" id="about-tea">
            <h3>Чайная суть <img src="{{ URL::to('assets/images/leaf2.png') }}"></h3>
            <p>Шесть плодов одного дерева:</p>
            <p><b>1. Бодрость</b></p>
            <p>Красный чай - это чай прошедший сильную ферментацию в процессе производства, для нас привычнее его называть чёрным чаем. Как правило, китайские красные чаи более мягкие по вкусу, и не имеют «индийской» терпкости. Бодрящий же эффект ощутим лишь в китайском красном чае, видимо это связано с процессом производства. Для производства качественного красного чая используют молодые чайные листья и почки. При этом, чем больше типсов (чайных почек) в чае, тем заметнее его бодрящий эффект. Типсовые красные чаи идеально подходят для хорошего начала дня!</p>
            <p><b>2. Гармония</b></p>
            <p>Улун – это полуферментированный чай. Многообразие сортов этого чая является гордостью Китая. Светлые улуны обладают свойством гармонизировать общение за чаепитием. Помогают настроиться на единый лад. Идеально подходят для деловых переговоров, встреч и свиданий.</p>
            <p><b>3. Отдых</b></p>
            <p>Запечённые улуны обладают эффектом снятия эмоционального напряжения. Настраивают на свободное общение, долгие задушевные беседы. Идеально подходят для дружеской компании.</p>
            <p><b>4. Тонус</b></p>
            <p>Зелёный чай давно известен своими полезными свойствами. Для производства качественного зеленого чая используют молодые чайные листья. Идеально подходит для восстановления сил после физических нагрузок. Проясняет разум, раскрывает творческий потенциал.</p>
            <p><b>5. Скорость</b></p>
            <p>Пуэр – это постферментированный чай, т.е. процессы ферментации проходит постепенно, в течение длительного времени. Поэтому ценятся пуэры выдержанные годами. Хороший пуэр определяется не только временем, но и качеством первоначального сырья и технологией изготовления. Пуэр бывает чёрным и зелёным. Чёрный Пуэр является отличным энергетиком. Имеет тёмный настой и мягкий вкус. В аромате могут быть ореховые, древесные или земляные нотки в зависимости от сорта. Повышает внимательность, работоспособность и активность в любое время суток. Также способствует усвоению пищи, улучшает обмен веществ, быстро снимает похмелье. Идеально подходит для выживания в большом городе.</p>
            <p><b>6. Динамичность.</b></p>
            <p>Зелёный (Шен) пуэр Вкус «Шен пуэра» слегка подкопченный, с тонким оттенком спелого крыжовника. Иной раз присутствуют фруктовые нотки! И как хорошая традиция у этого чая - сладкое, немного вяжущие послевкусие. Этот чай многообразен и богат на оттенки вкусов, раскрывая их поэтапно во время заваривания.

                Его важно воспринимать как коллекционный чай, так как вкус «шен пуэра» со временем ощутимо меняется, приобретая новые нотки вкуса!

                Совсем опрометчиво выпить его задолго перед тем как добудете еще несколько экземпляров с других знаменитых гор или провинций!</p>
            <h4>Как правильно заварить вкусный ЧАЙ.</h4>
            <p><b>1. Традиционный способ.</b></p>
            <p>Одним из традиционных способов приготовления чая является пролив. Этот способ как нельзя лучше подходит для заваривания китайских сортов чая. Для получения чая используют два сосуда: в одном заваривают чай, а затем переливают готовый настой во второй сосуд. Каждую последующую заварку настаивают чуть дольше предыдущей.</p>
            <p><b>2. Простой способ.</b></p>
            <p>Один из самых простых способов заварить чай – это заварить его прямо в кружку. Главное соблюсти тонкий момент первенства воды и чая: красные и чёрные чаи насыпают в кружку и заливают резким кипятком, а зелёные чаи и улуны лучше опустить в уже налитый в кружку кипяток. Количество заварки необходимое на кружку 2 грамма – обычно, это одна щепотка.</p>
        </div>
        <div class="item col-9 col-sm-8" id="about-coffee">
            <h3>О Кофе – самом популярном напитке во всем Мире! <img src="{{ URL::to('assets/images/coffee2.png') }}"></h3>
            <p>Мы продаем кофе класса Specialty.</p>
            <p>Кофе, представленный в нашем ассортименте можно варить, а можно заваривать.</p>
            <p><b>Кофе, подходящий для варки:</b><br>
            Эспрессо, смеси и моносорта с обжаркой под эспрессо, порадуют вас своим балансом и яркостью вкуса.</p>
            <p><b>Кофе, подходящий для заварки:</b><br>
            Это моносорта для альтернативных способов приготовления.</p>
            <p>Вы полюбите нотки и оттенки во вкусе присущие каждому кофейному сорту, ощутить которые возможно только выбрав Заварку как способ приготовления!</p>
            <h4>Как приготовить вкусный Кофе!</h4>
            <p><b>1. Кофе можно сварить:</b></p>
            <p>в Турке, Гейзере, Сифоне, Кофеварке.
                Советуем использовать кофе -
                Специально созданные для варки эспрессо смеси
                или моносорта с обжаркой под эспрессо.</p>
            <p><b>1. Кофе можно заварить:</b></p>
            <p>Это самый древний, быстрый и удобный способ приготовить кофе.
                На обычную КРУЖКУ или Френч-пресс, кемикс, аэропрес на 350 мл потребуется 2,5 – 3 чайные ложки с небольшой горкой молотого кофе. Измельчать кофе лучше всего непосредственно перед приготовлением.</p>
            <p>В предварительно прогретую посуду засыпьте кофе и залейте кипятком (100 градусов) с небольшого расстояния от ёмкости. Это позволит насытить воду кислородом!</p>
            <p>Спустя 30 секунд размешайте ложечкой КОФЕ.</p>
            <p>В Случае с Френч-прессом, поршень можно опустить.</p>
            <p>В Кемексе и Аэропресе кофе не размешивают.</p>
            <p>Сахар, сливки или мороженое по вкусу возможно добавить в последнею очередь.</p>
            <p>Вуаля! – Вкусный КОФЕ готов!</p>
        </div>
        <div class="item col-9 col-sm-8" id="about-us">
            <h3>О нас <img src="{{ URL::to('assets/images/pot.png') }}"></h3>
            <p>Мы существуем с 2010 года.
                По сей прекрасный день. Всё это время
                мы стремились к максимальному качеству
                и безупречности предлагаемой нами
                продукции.
                Индивидуальный подход к каждому покупателю
                стал фундаментом, приносящим нам
                спелые плоды - Вас, наших постоянных
                клиентов.</p>
        </div>
    </div>
    <div class="container mt-4 mb-1" id="ya-map">
        <div class="map_point2"><img src="{{ URL::to('assets/images/map_point2.png') }}"></div>
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A09aef0c49cf4adfb0ac4a63264ead5ec8174b1aea6f6a2e9613c444b7eeda597&amp;width=100%25&amp;height=700&amp;lang=ru_RU&amp;scroll=false"></script>
    </div>
</main>
</body>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/scripts.js"></script>
</html>