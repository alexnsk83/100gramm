


<div class="boxed  push-down-60">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
            <div class="post-content">
                <h1>Авторизация</h1>
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <form method="post">
                    {{ csrf_field() }}
                    <input type="text" name="name" placeholder="{{ trans('site.form.name') }}" value="{{ old('name') }}"><br>
                    <input type="password" name="password" placeholder="{{ trans('site.form.password') }}"><br>
                    <input type="checkbox" id="remember" name="remember"><label for="remember"><span>{{ trans('site.form.remember') }}</span></label><br>
                    <input type="submit" name="auth" value="{{ trans('site.button.login') }}"><br>
                </form>
                @if (session('authError'))
                    <p class="red">{{ session('authError') }}</p>
                @endif
            </div>
        </div>
    </div>
</div>
