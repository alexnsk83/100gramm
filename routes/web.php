<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get( '/', 'MainController@index')->name('site.home');

Route::post('/', "MainController@sendForm")->name('home.sendForm');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', 'AuthController@login')->name('site.auth.login');
    Route::post('/login', 'AuthController@loginPost')->name('site.auth.loginPost');
});

Route::get('/logout', 'AuthController@logout')->name('site.auth.logout');

Route::get('/page/{slug}', 'PageController@showPage')->name('site.page');

//Admin routes
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'isAdmin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.home');

    //Post
    Route::group(['prefix' => 'post'], function () {
        Route::get('/', 'PostController@showAll')->name('admin.post.all');
        Route::get('/trashed', 'PostController@showTrashed')->name('admin.post.trashed');

        Route::get('/add', 'PostController@add')->name('admin.post.add');
        Route::post('/add', 'PostController@addPost')->name('admin.post.addPost');

        Route::get('/edit/{id}', 'PostController@edit')->name('admin.post.edit')->where('id', '[0-9]+');
        Route::post('edit/{id}', 'PostController@editPost')->name('admin.post.edit')->where('id', '[0-9]+');

        Route::get('/delete/{id}', 'PostController@delete')->name('admin.post.delete')->where('id', '[0-9]+');
        Route::get('/recover/{id}', 'PostController@recover')->name('admin.post.recover')->where('id', '[0-9]+');
    });

    //Orders
    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'OrderController@showAll')->name('admin.order.all');

        Route::get('/{id}','OrderController@edit')->name('admin.order.edit');
    });

    //Sale
    Route::group(['prefix' => 'sale'], function () {
        Route::get('/', 'SaleController@showAll')->name('admin.sale.all');
        Route::get('/trashed', 'SaleController@showTrashed')->name('admin.sale.trashed');

        Route::get('/add', 'SaleController@add')->name('admin.sale.add');
        Route::post('/add', 'SaleController@addPost')->name('admin.sale.addPost');

        Route::get('/edit/{id}', 'SaleController@edit')->name('admin.sale.edit')->where('id', '[0-9]+');
        Route::post('edit/{id}', 'SaleController@editPost')->name('admin.sale.edit')->where('id', '[0-9]+');

        Route::get('/delete/{id}', 'SaleController@delete')->name('admin.sale.delete')->where('id', '[0-9]+');
        Route::get('/recover/{id}', 'SaleController@recover')->name('admin.sale.recover')->where('id', '[0-9]+');
    });

    //Options
    Route::group(['prefix' => 'options'], function () {
        Route::get('/', 'OptionController@edit')->name('admin.options');

        Route::post('/','OptionController@editPost')->name('admin.options.edit');
    });
});

